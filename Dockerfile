# Event Data Crossref Agents

FROM clojure:lein-2.7.0-alpine
MAINTAINER Joe Wass jwass@crossref.org

COPY target/uberjar/*-standalone.jar ./agents.jar

ENTRYPOINT ["java", "-jar", "agents.jar"]
